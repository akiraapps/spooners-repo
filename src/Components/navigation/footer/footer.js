import React from 'react';
import './footer.css';
import { Container, Col, Row } from 'reactstrap';
import fb from '../../../Assests/icons/footer/fb.png'; 
const footer = () => {
    return (
        <footer id="footer" className="footer">
            <Container>
                <Row> 
                    <Col>
                        <div className="storage2"> 
                            <a href="https://www.facebook.com/justin.spoon.18041" ><img src={fb} alt="Icon" /></a> 
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="footer-text">Contact: 99999999 <br />
                            &copy; COPYRIGHT 2021 reserved by Spooners<br /><br/></p>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
}
export default footer;